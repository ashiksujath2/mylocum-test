# import multiprocessing

bind = '127.0.0.1:8002'
workers = 2
daemon = True
pidfile = 'gunicorn_process_id.txt'
accesslog = 'gunicorn_access_log.txt'
errorlog = 'gunicorn_error_log.txt'
loglevel = 'debug'
worker_class = 'gevent'
timeout = 360  # decrease this value and see what happens
