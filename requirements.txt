Django==1.8.2
gevent==1.0.2
gnureadline==6.3.3
greenlet==0.4.7
gunicorn==19.3.0
ipython==3.1.0
wsgiref==0.1.2
