import json

from django.views.generic import View
from django.http import Http404, HttpResponse


class TestView(View):

    def get(self, request, *args, **kwargs):
        data = {
            'status': 'ok',
            'message': 'hello world'
        }
        return HttpResponse(json.dumps(data), content_type='application/json')
